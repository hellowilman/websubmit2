from django.shortcuts import render
# Create your views here.
from django.core.urlresolvers import reverse

from django import forms
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.http import HttpResponse
from utils import ResumableBackend
from utils.webtools import *
from django.contrib.auth.models import User
from Main.models import Register
from utils.sendemail import  GmailSendEmail

def bfs_contest2015_view(request):
    if request.method =="POST":
        print request.POST
        email = get_query(request,"email")
        tel = get_query(request,"tel")
        name = get_query(request,"name")
        key = get_query(request,"key")
        users = User.objects.filter(email = email)
        if users:
            user = users[0]
        else:
            user = User.objects.create_user(email,email,"123456",first_name = name)
            user.save()
            reg = Register(user = user,key = key, tel = tel)
            reg.save()
            s = GmailSendEmail("bfschoolhk","bfs@2014!")
            s.send([email],'nihao',"this is a very good testing email")



        return render_to_response("bfs_contest2015.html")
    else:
        return render_to_response("bfs_contest2015.html")
def myUploadView2(request):
    R = ResumableBackend.Resumable()
    if request.method == "POST":
        file_id = get_query(request,"resumableIdentifier")
        chunk_id = get_query(request,"resumableChunkNumber")
        file_blob_size = get_query(request,"resumableCurrentChunkSize")
        file_name =  get_query(request,"resumableFilename")
        chunk_num = get_query(request,"resumableTotalChunks")
        token = get_query(request,"upload_token")
        file_blob = get_query(request,"file")
        R.handle_chunk(token, file_blob,file_id,file_name, int(file_blob_size), chunk_id,chunk_num)
        return HttpResponse("OK",status = 200)

    else:

        file_id = get_query(request,"resumableIdentifier")
        chunk_id = get_query(request,"resumableChunkNumber")
        file_blob_size = get_query(request,"resumableCurrentChunkSize")
        chunk_num = get_query(request,"resumableTotalChunks")
        token = get_query(request,"upload_token")
        status = False
        if token and file_id and chunk_id and chunk_num and file_blob_size:
            status = R.check_chunk_status(token,file_id,chunk_id,chunk_num,file_blob_size)
            if not status:
                return HttpResponse("",status = 201)
            else:
                return HttpResponse("",status = 200)
        else:
            return render_to_response("myupload.html")